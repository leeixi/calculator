const keyboard = document.getElementById('keyboard')
const output = document.getElementById('output-value')

keyboard.addEventListener('click', clear)
keyboard.addEventListener('click', backspace)
keyboard.addEventListener('click', getValue)
keyboard.addEventListener('click', getResult)

let checkInput = false
let checkOperator = false

const operators = ['%', '/', '*', '+', '-']

function getValue(e) {
  if (e.target.id.length <= 1 && e.target.id !== '=') {
    if (checkInput) {
      output.value = ''
      checkInput = false
    }
    output.value += e.target.textContent

    const secondLastNum = output.value[output.value.length - 2]
    for (const i of operators) {
      if (secondLastNum.includes(i)) {
        output.value = output.value.slice(0, -1) + e.target.value
      }
    }
  }
}

function clear(e) {
  if (e.target.id === 'clear') {
    output.value = ''
  }
}

function backspace(e) {
  if (e.target.id === 'backspace') {
    output.value = output.value.slice(0, -1)
  }
}

function getResult(e) {
  if (e.target.id === '=' && output.value.length > 0) {
    let result = eval(output.value)
    output.value = result
    checkInput = true
  }
}
